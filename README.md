# Cache Project

### Team Members:
- Brandon Cole, Junior
- Daniel Erhabor, Junior
- David Awogbemila, Junior
- Michaun Pierre, Sophomore
- Temilola Oloyede, Junior

## Running The Program
The program can be compiled "gcc -o processor single-cycle-with-cache.c cpu.c syscall.c memory.c" 
or the make command can simply be called. The program can then be called as follows: $processor program.sim

The program initially displays the instructions and data and prompts the user to press enter to run the next cycle.
Press enter to run the next cycle.

## Workload Distribution
- Brandon - write write logic - set-associative
- Daniel - Implement instruction cache structure, write read logic - direct mapped
- David - Implement data cache structure, write read logic - set associative
- Michaun - Implement instruction cache structure, write read logic - direct mapped
- Temilola - testing


## Test Cases

#### Test Data Cache Read Miss - In folder util/test_2_Read_Miss
Input: 

~~~~
	lw $t3, 0($s1)
~~~~
	
Expected Output:

	data cache read miss
	
Actual Output

	data cache read miss

#### Test Cache Read Hit - In folder util/test_3_Read_hit
Input: 
~~~~
	sw $t1, 0($s1)
	lw $t3, 0($s1)	
	lw $t4, 0($s1)
~~~~
	
Expected Output:

	data cache write miss
	data cache read hit	
	data cache read hit
	
Actual Output:

	data cache write miss
	data cache read hit
	data cache read hit
	
#### Instruction Cache  - In folder util/test_4_I$_Test
Input: 
~~~~
	addi $s1, $0, 128   # Miss
	addi $t1, $0, 64    # Hit	
	sll $s1, $s1, 2		# Hit	
	sll $t0, $s1, 2     # Hit
	
	addi $s1, $0, 128   # Miss
	addi $t1, $0, 64    # Hit
	sll $s1, $s1, 2		# Hit	
	sll $t0, $s1, 2     # Hit
~~~~
	
Expected Output:

	Instruction cache Miss
	Instruction cache Hit
	Instruction cache Hit
	Instruction cache Hit
	
	Instruction cache Miss
	Instruction cache Hit
	Instruction cache Hit
	Instruction cache Hit
	
Actual Output: 

	Instruction cache Miss
	Instruction cache Hit
	Instruction cache Hit
	Instruction cache Hit
	
	Instruction cache Miss
	Instruction cache Hit
	Instruction cache Hit
	Instruction cache Hit

#### Test Data Cache Read and Write  - In folder util/test_5_D_W
Input: 
~~~~
	sw $t1, 0($s1)	
	lw $t3, 0($s1)	
	lw $t4, 0($s1)	
	addi $t3, $0, 7
	sw $t3, 0($s1)
	lw $t5, 0($s1)
~~~~
	
Expected Output:

	data cache write miss
	data cache read hit
	data cache read hit
	n/a
	data cache write hit
	data cache read hit
	
Actual Output:

	data cache write miss
	data cache read hit
	data cache read hit
	n/a
	data cache write hit
	data cache read hit

## Notes From Team Meetings
![Image](https://i.imgur.com/H6l7ekG.jpg)
![Image](https://i.imgur.com/Qnft0WF.jpg)
![Image](https://i.imgur.com/DOf4X5o.jpg)
